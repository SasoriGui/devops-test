from flask import Flask
from flask_httpauth import HTTPTokenAuth
import socket , os

app = Flask(__name__)
auth = HTTPTokenAuth()

# TODO: use environment variables to prevent this secret to leak.
#secret_token = "ThisShouldBeSecret"
secret_token = os.getenv('APP_SECRET_KEY')
@auth.verify_token
def verify_token(token):
    return token == secret_token


@app.route("/", methods=["GET"])
@auth.login_required
def home():
    return {
        "message": "Your Flask API server is flying!"
    }
#This was added to validate the loadbalancing of the app
@app.route("/lb", methods=["GET"])
@auth.login_required
def index():
    return "This is your loadbalancing working ! My Hostname is: %s \n" % (socket.gethostname())

if __name__ == "__main__":
    app.run("127.0.0.1", port=8000)
