FROM python:3.8-alpine
WORKDIR .

ENV FLASK_APP=app.py
ENV FLASK_RUN_HOST=0.0.0.0


ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk add --no-cache gcc musl-dev linux-headers
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
EXPOSE 8000
COPY . .
CMD ["python", "api/app.py"]


# Use this file as a starting point to create
# a production-ready Dockerfile for our Flask API.
#
# Bonus point: take advantage of Docker's Multi-Stage Build
# to make this single Dockerfile ready for both development
# and production environments.
